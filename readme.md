# ADH Proof of Concept
Diese Repo dient nur dem Ausprobieren von möglichen Technologien für das ADH Projekt.
Das Repo beinhaltet sowohl Backend als auch Frontend.

## Einrichten
### Node installieren
Wir benötigen node Version 12.18.2. Das geht am besten mit dem nvm (Node Version Manager)
#### nvm installieren
``` 
$ curl -o- https://raw.githubusercontent.com/creationix/nvm/v0.33.11/install.sh | bash
$ source ~/.bashrc	
```
Der zweite Befehl ist eventuell nicht notwendig bzw. lässt sich auch durch einen Neustart des 
Terminals ersetzen.

#### node mit nvm installieren
```
$ nvm ls-remote
$ nvm install 12.18.2
$ node -v
```
Node -v sollte jetzt die korrekte Version ausgeben (v12.18.2)

## Repo auscheken
```
$ git clone https://bitbucket.org/jospahn/ad_poc.git
```

### Backend 
```
$ cd ad_poc/backend
$ npm install             
$ touch .env              // Datei für Umgebungsvariablen
$ echo PORT=xxxx > .env   // Portnummer als Umgebungsvariable (xxxx z.B. durch 3000 ersetzen)
```

### Frontend
+ tbd

## Ausführen
### Backend
Folgende Befehl im ordner backend/ ausführen.
Im Entwicklungsmodus (mit ts -> js transpiling und Änderungsüberwachung):
```
$ npm run dev
```

Die übersetzte js Datei lässt sich auch direkt ausführen:
```
$ node dist/index.js
```
